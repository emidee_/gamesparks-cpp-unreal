using UnrealBuildTool;
using System.IO;
using System;

public class OnlineSubsystemGameSparks : ModuleRules
{
    // Api changed by epic - activate the appropriate line
    public OnlineSubsystemGameSparks(TargetInfo Target) // < 4.15
    // public OnlineSubsystemGameSparks(ReadOnlyTargetRules Target) : base(Target) // > 4.15
    {
        Definitions.Add("GAMESPARKS_PACKAGE=1");
        Definitions.Add("ONLINESUBSYSTEMGAMESPARKS_PACKAGE=1");

        PrivateDependencyModuleNames.AddRange(
            new string[] {
                "Core",
                "CoreUObject",
                "OnlineSubsystem",
                "Engine",
                "Json",
                "GameSparks",
            }
        );

        PrivateIncludePaths.Add("Private");
    }

    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string GameSparksPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "..", "GameSparksBaseSDK")); }
    }
}
